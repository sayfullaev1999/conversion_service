from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from core.conf import settings


def register_app() -> FastAPI:
    # FastAPI
    app = FastAPI(
        debug=settings.DEBUG,
        title=settings.TITLE,
        version=settings.VERSION,
        description=settings.DESCRIPTION,
    )

    # Register middleware
    register_middleware(app)

    return app


def register_middleware(app: FastAPI) -> None:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=['*'],
        allow_credentials=True,
        allow_methods=['*'],
        allow_headers=['*'],
    )
